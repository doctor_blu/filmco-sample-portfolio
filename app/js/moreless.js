// SHOW MORE TEXT
(function() {

	var mores = document.querySelectorAll("div.more");

	var expand = function(e) {

		var button = e.target;
		// more -> .post__info -> article -> post__more
		var post__more = button.parentNode.parentNode.children[2];

		var post__intro__text = button.parentNode.children[3];

		post__more.className += " post__more--visible";
		button.className += " hidden";
		post__intro__text.className += " post__intro__text--moreText";

		// window.scrollBy(0, post__intro__text.offsetHeight);
	};

	for (var i = 0; i < mores.length; i++) {
		mores[i].addEventListener('click', expand, false);
	}

})();


// SHOW LESS TEXT
(function() {

	var lesses = document.querySelectorAll("div.less");
	var mores = document.querySelectorAll("div.more");

	var moreOriginalClass = [];
	for (var j = 0; j < mores.length; j++) {
		moreOriginalClass.push(mores[j].className);
	}

	var post__moreOriginalClass = document.querySelectorAll("section.post__more")[0].className;
	var post__intro__textOriginalClass = document.querySelectorAll("div.post__intro__text")[0].className;


	var contract = function(e) {

		var k = 0;

		var button = e.target;
		// less -> .post__more -> article -> post__intro -> button More
		var more = button.parentNode.parentNode.children[1].children[4];
		
		//taking the right class to re-apply to the more button (different colors)
		for (var i = 0; i < mores.length; i++) {
			if (mores[i].isEqualNode(more)) {
				k = i;
			}
		}
		// less -> .post__more
		var post__more = button.parentNode;
		// less -> .post__more -> article -> post__intro -> post__intro__text
		var post__intro__text = button.parentNode.parentNode.children[1].children[3];

		post__more.className = post__moreOriginalClass;
		more.className = moreOriginalClass[k];
		post__intro__text.className = post__intro__textOriginalClass;

		window.scrollBy(0, -post__more.offsetHeight);
	};

	for (var i = 0; i < lesses.length; i++) {
		lesses[i].addEventListener('click', contract, false);
	}

})();