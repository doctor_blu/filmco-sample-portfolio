// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

// MIT license

(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}

	if (!window.requestAnimationFrame) {
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() {callback(currTime + timeToCall); }, timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}

	if (!window.cancelAnimationFrame) {
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
	}
}());

// ONSCROLL 
(function() {
	var lastScrollY = 0;
	var ticking = false;
	var header = document.getElementById("topbar");
	var originalClass = header.className;
	var threshold = 32;

	var update = function() {
		// In do your stuff section
		if (lastScrollY > threshold) {
			header.className = originalClass + " mainHeader--collapsed";
		} else {
			header.className = originalClass;
		}
		ticking = false;
	};

	var requestTick = function() {
		if (!ticking) {
			window.requestAnimationFrame(update);
			ticking = true;
		}
	};

	var onScroll = function() {
		lastScrollY = window.scrollY;
		requestTick();
	};

	window.addEventListener('scroll', onScroll, false);
})();