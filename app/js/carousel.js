// Simple slider plugin, Marco Ciampini

// How to setup HTML:

/*
	<section id="CAROUSEL_ID">
		<div class="carouselSetup">
			<img src="FALLBACK_IMAGE_SRC" alt="">
			<ul>
				<li><div class="carouselPlaceholder" data-image="FIRST_IMAGE_SCR"></div></li>
				<li><div class="carouselPlaceholder" data-image="SECOND_IMAGE_SCR"></div></li>
				<li><div class="carouselPlaceholder" data-image="THIRD_IMAGE_SCR"></div></li>
			</ul>
		</div>
	</section>
*/

//How to start the plugin:

/*

	window.addEventListener(
		"load",
		function() {
			var opt = new sliderOptions({
				buttonBgColor: "green" (defaul value: "red", this value represent a class who gives the background-color property to an element)
			});
			var lb = new slider("CAROUSEL_ID", opt);
		},
		false
	);

*/


(function() {

	var slider, sliderOptions;

	sliderOptions = (function() {

		function sliderOptions(opt) {
			var options = opt || {};
			this.buttonBgColor = options.buttonBgColor || "red";
		}

		return sliderOptions;

	})();

	slider = (function() {

		var externalContainer,
			internalContainer,
			loader,
			prevButton,
			nextButton;


		var startingPositionAnimation = null;
		var finalPositionAnimation = null;
		var framesToCompleteAnimation = 45; //higher the number, slower the animation

		var placeholderClass = "carouselPlaceholder";
		var internalContainerClass = "carouselContainer";
		var imgContainerClass = "carousel_imgContainer";
		var setupElementClass = "carouselSetup";

		function slider(id, options) {
			externalContainer = document.getElementById(id);
			if (!externalContainer) {
				console.log("Unable to find carousel externalContainer element");
				return;
			}
			this.options = options;
			this.album = []; //contains the imgs urls
			this.currentImage = -1;
			this.init();
		}

		slider.prototype.init = function() {
			if (this.createAlbum()) {
				this.buildStructure();
			}
		};

		slider.prototype.resizeElements = function(__this) {
			var _this = __this;

			var imgContainer;
			internalContainer.style.width = (externalContainer.clientWidth*_this.album.length)+"px";
			internalContainer.style.height = externalContainer.clientHeight+"px";
			internalContainer.style.left = -(_this.currentImage*externalContainer.clientWidth)+"px";

			for (var i=0; i<internalContainer.children.length; i++) {
				imgContainer = internalContainer.children[i];
				imgContainer.style.width = externalContainer.clientWidth+"px";
				imgContainer.style.height = externalContainer.clientHeight+"px";
			}
		};

		slider.prototype.buildStructure = function() {
			
			var _this = this;

			var fragment = document.createDocumentFragment();
			var imgContainer, img;

			internalContainer = document.createElement("div");
			internalContainer.className = internalContainerClass;
			//internalContainer.style.width = (externalContainer.clientWidth*_this.album.length)+"px";
			//internalContainer.style.height = externalContainer.clientHeight+"px";
			
			//PLUS: center images in container

			for (var i=0; i<_this.album.length; i++) {
				imgContainer = document.createElement("div");
				imgContainer.className = imgContainerClass;
				//imgContainer.style.width = externalContainer.clientWidth+"px";

				img = document.createElement("img");
				img.className = "imageCenteredInContainer";
				img.src = _this.album[i];

				imgContainer.appendChild(img);
				internalContainer.appendChild(imgContainer);
			}


			prevButton = document.createElement("div");
			prevButton.className = "button white carousel_prev bg-"+_this.options.buttonBgColor;
			prevButton.innerHTML = "<";

			nextButton = document.createElement("div");
			nextButton.className = "button white carousel_next bg-"+_this.options.buttonBgColor;
			nextButton.innerHTML = ">";

			var controlsContainer = document.createElement("div");
			controlsContainer.className = "carousel_controlsContainer";

			controlsContainer.appendChild(prevButton);
			controlsContainer.appendChild(nextButton);

			fragment.appendChild(internalContainer);
			fragment.appendChild(controlsContainer);

			externalContainer.children[0].style.display = "none";

			_this.resizeElements(_this);
			_this.centerImagesInContainer();

			_this.checkArrowsVisibility(_this);
			prevButton.addEventListener("click",
				function(event) {
					_this.prevImage(_this);
				},
				false
			);
			nextButton.addEventListener("click",
				function(event) {
					_this.nextImage(_this);
				},
				false
			);

			externalContainer.appendChild(fragment);

			window.addEventListener(
				"resize",
				function() {
					_this.resizeElements(_this);
					_this.centerImagesInContainer();
				},
				false
			);

		};

		slider.prototype.createAlbum = function() {

			var _this = this;
			_this.album = [];
			_this.currentImage = -1;

			// externalContainer -> carouselSetup -> ul -> li s
			var placeholders = document.querySelectorAll("#"+externalContainer.id+" ."+setupElementClass+" ul li");


			for (var i=0; i<placeholders.length; i++) {
				if (placeholders[i].children[0].className.indexOf(placeholderClass)>=0) {
					_this.album.push(placeholders[i].children[0].getAttribute('data-image'));
				}
			}

			if (_this.album.length > 0) {
				_this.currentImage = 0;
				return true;
			} else {
				return false;
			}
		};

		// PREV-NEXT IMAGE
		slider.prototype.prevImage = function(__this) {
			var _this = __this;
			_this.switchImage(_this, -1);
		};
		slider.prototype.nextImage = function(__this) {
			var _this = __this;
			_this.switchImage(_this, +1);
		};
		slider.prototype.switchImage = function(__this, i) {
			var _this = __this;
			var albumLength = _this.album.length;
			if (albumLength>0) {
				if ((i>0 && _this.currentImage<albumLength-1) ||
					(i<0 && _this.currentImage>0)) {
					_this.currentImage += i;
					//internalContainer.style.left = -(_this.currentImage*externalContainer.clientWidth)+"px";
					if (i>0) {
						animateSlideNext();
					} else if (i<0) {
						animateSlidePrev();
					}
					_this.checkArrowsVisibility(_this);
				}
			}
		};
		slider.prototype.checkArrowsVisibility = function(__this) {
			var _this = __this;
			var i = _this.currentImage;
			var l = _this.album.length;

			if (i<1) {
				prevButton.style.display = "none";
			} else {
				prevButton.style.display = "block";
			}
			if (i>l-2) {
				nextButton.style.display = "none";
			} else {
				nextButton.style.display = "block";
			}
		};

		slider.prototype.centerImagesInContainer = function() {
			var imgs = document.querySelectorAll("."+internalContainerClass+" img.imageCenteredInContainer");

			for (var i = 0; i < imgs.length; i++) {
				//image
				var w = imgs[i].clientWidth;
				var h = imgs[i].clientHeight;
				//container
				var _w = imgs[i].parentNode.clientWidth;
				var _h = imgs[i].parentNode.clientHeight;

				//left and right values to apply
				var l = (_w - w)/2;
				var t = (_h - h)/2;

				// if (l<0) {
					imgs[i].style.left = l+"px";
				// }
				// if (t<0) {
					imgs[i].style.top = t+"px";
				// }
			}
		};

		var animateSlideNext = function() {
			
			var currentOffset = parseInt(internalContainer.style.left);

			if (!startingPositionAnimation) {
				startingPositionAnimation = currentOffset;
			}
			if (!finalPositionAnimation) {
				finalPositionAnimation = startingPositionAnimation-externalContainer.clientWidth;
			}

			if (currentOffset<=finalPositionAnimation) {
				internalContainer.style.left = finalPositionAnimation+"px";
				startingPositionAnimation = null;
				finalPositionAnimation = null;
			} else {
				requestAnimationFrame(animateSlideNext);

				var speed=externalContainer.clientWidth/framesToCompleteAnimation;

				internalContainer.style.left = (currentOffset-speed)+"px";
			}
		};
		var animateSlidePrev = function() {
			
			var currentOffset = parseInt(internalContainer.style.left);

			if (!startingPositionAnimation) {
				startingPositionAnimation = currentOffset;
			}
			if (!finalPositionAnimation) {
				finalPositionAnimation = startingPositionAnimation+externalContainer.clientWidth;
			}

			if (currentOffset>=finalPositionAnimation) {
				internalContainer.style.left = finalPositionAnimation+"px";
				startingPositionAnimation = null;
				finalPositionAnimation = null;
			} else {
				requestAnimationFrame(animateSlidePrev);

				var speed=externalContainer.clientWidth/framesToCompleteAnimation;
				internalContainer.style.left = (currentOffset+speed)+"px";
			}
		};

		return slider;

	})();

	window.addEventListener(
		"load",
		function() {
			var opt = new sliderOptions({
				buttonBgColor: "green"
			});
			var lb = new slider("myCarousel", opt);
		},
		false
	);
})();