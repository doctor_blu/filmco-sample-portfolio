//CENTER IMAGES WITH RESPECT TO THE CONTAINER (Gallery)

(function() {

	var imgs = document.querySelectorAll(".galleryContainer img.imageCenteredInContainer");

	var fit = function() {

		for (var i = 0; i < imgs.length; i++) {
			//image
			var w = imgs[i].clientWidth;
			var h = imgs[i].clientHeight;
			//container
			var _w = imgs[i].parentNode.clientWidth;
			var _h = imgs[i].parentNode.clientHeight;

			//left and right values to apply
			var l = (_w - w)/2;
			var t = (_h - h)/2;

			// if (l<0) {
				imgs[i].style.left = l+"px";
			// }
			// if (t<0) {
				imgs[i].style.top = t+"px";
			// }
		}

	};

	window.addEventListener("load", fit, false);
	window.addEventListener("resize", fit, false);
	window.addEventListener("orientationchange", fit, false);

})();