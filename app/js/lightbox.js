// Lightbox plugin, exploting the background-image,
// background-size and background-position properties
// for block elements


// the HTML structure for the thumbnails should be:

/*

<div class="galleryContainer">
	<a href="path-to-large-image">
		<img src="path-to-thumbnail" alt="">
	</a>
</div>

*/


// When clicking on an image, siblings will automatically
// be added in the album

//Animations are not included - this plugin is pure
// JS, to avoid jQuery overhead



(function() {

	var lightBox, lightBoxOptions;

	lightBoxOptions = (function() {

		function lightBoxOptions(opt) {
			var options = opt || {};
			// no options
		}

		return lightBoxOptions;

	})();

	lightBox = (function() {

		var container,
			loader,
			prevButton,
			nextButton;
		var thumbnailClass = "galleryContainer";

		function lightBox(options) {
			this.options = options;
			this.album = []; //contains the <img> thumbs nodes
			this.currentImage = -1;
			this.init();
		}

		lightBox.prototype.init = function() {
			this.activateListeners();
			this.buildStructure();
		};

		lightBox.prototype.activateListeners = function() {
			var _this = this;
			var thumbnails = document.querySelectorAll("."+ thumbnailClass + " img");

			for (var i=0; i<thumbnails.length; i++) {
				thumbnails[i].addEventListener('click',
					function(event) {
						_this.show(_this, event);
					},
					false
				);
			}
		};

		lightBox.prototype.buildStructure = function() {
			
			var _this = this;

			//create separate html elements
			var fragment = document.createDocumentFragment();
			
			var lb_container = document.createElement("div");
			lb_container.className = "lightBox_Container";

			container = document.createElement("div");
			container.className = "lightBox_imageContainer";

			loader = document.createElement("div");
			loader.className = "lightBox_loader";

			var closeContainer = document.createElement("div");
			closeContainer.className = "lightBox_closeContainer";

			prevButton = document.createElement("div");
			prevButton.className = "button lightBox_prev white bg-blue";
			prevButton.innerHTML = "<";

			nextButton = document.createElement("div");
			nextButton.className = "button lightBox_next white bg-blue";
			nextButton.innerHTML = ">";

			var closeButton = document.createElement("div");
			closeButton.className = "button lightBox_close white bg-red";
			closeButton.innerHTML = "X";

			//create HTML structure in a separate fragment
			closeContainer.appendChild(closeButton);
			closeContainer.appendChild(prevButton);
			closeContainer.appendChild(nextButton);
			lb_container.appendChild(loader);
			lb_container.appendChild(container);
			lb_container.appendChild(closeContainer);
			fragment.appendChild(lb_container);

			//append fragment to body
			var body = document.querySelector("body");
			body.appendChild(fragment);

			// add listener to buttons
			closeButton.addEventListener("click", _this.close, false);
			prevButton.addEventListener("click",
				function(event) {
					_this.prevImage(_this);
				},
				false
			);
			nextButton.addEventListener("click",
				function(event) {
					_this.nextImage(_this);
				},
				false
			);
			// add listener on ESC button
			body.addEventListener("keydown", function(e) {
				var ESC_keyCode = 27;

				if (e.keyCode === ESC_keyCode) {
					_this.close();
				}

			}, false);
		};

		lightBox.prototype.createAlbum = function(__this, clickedImage) {

			var _this = __this;
			_this.album = [];
			_this.currentImage = -1;

			// search for siblings
			// img -> a -> imageContainer -> albumContainer -> children
			var siblings = clickedImage.parentNode.parentNode.parentNode.children;

			for (var i=0; i<siblings.length; i++) {
				if (siblings[i].className.indexOf(thumbnailClass)>=0) {
					_this.album.push(siblings[i].children[0].children[0]);
					if (siblings[i].isEqualNode(clickedImage.parentNode.parentNode)) {
						_this.currentImage = i;
					}
				}
			}
		};

		lightBox.prototype.changeBgImage = function(src) {
			container.style.backgroundImage = "url("+src+")";
			container.style.display = "block";
		};

		lightBox.prototype.loadNewImage = function(__this, src) {

			var _this = __this;

			container.parentNode.style.display = "block";
			container.style.display = "none";

			var large = new Image();

			if (src !== null &&
				src !== undefined) {

				large.onload = _this.changeBgImage(src);
				large.src = src;
				_this.checkArrowsVisibility(_this);
			}
		};

		lightBox.prototype.show = function(__this, e) {

			e.preventDefault();
			
			loader.style.display = "block";

			var thumb = e.target;
			var _this = __this;

			_this.createAlbum(_this, e.target);
			_this.loadNewImage(_this, thumb.parentNode.href);
			
		};

		lightBox.prototype.close = function(t) {

			if (container.parentNode.style.display === "block" ) {
				container.style.backgroundImage = "";
				container.style.display = "none";
				loader.style.display = "none";
				container.parentNode.removeAttribute("style");
			}
		};

		// PREV-NEXT IMAGE
		lightBox.prototype.prevImage = function(__this) {
			var _this = __this;
			_this.switchImage(_this, -1);
		};
		lightBox.prototype.nextImage = function(__this) {
			var _this = __this;
			_this.switchImage(_this, +1);
		};
		lightBox.prototype.switchImage = function(__this, i) {
			var _this = __this;
			var albumLength = _this.album.length;
			if (albumLength>0) {
				if ((i>0 && _this.currentImage<albumLength-1) ||
					(i<0 && _this.currentImage>0)) {
					_this.currentImage += i;
					_this.loadNewImage(_this, _this.album[_this.currentImage].parentNode.href);
					_this.checkArrowsVisibility(_this);
				}
			}
		};

		lightBox.prototype.checkArrowsVisibility = function(__this) {
			var _this = __this;
			var i = _this.currentImage;
			var l = _this.album.length;

			if (i<1) {
				prevButton.style.display = "none";
			} else {
				prevButton.style.display = "block";
			}
			if (i>l-2) {
				nextButton.style.display = "none";
			} else {
				nextButton.style.display = "block";
			}
		};

		return lightBox;

	})();

	window.addEventListener(
		"load",
		function() {
			var opt = new lightBoxOptions();
			var lb = new lightBox(opt);
		},
		false
	);
})();