var map;
function initialize() {

	var sohoOffice = new google.maps.LatLng(51.512775, -0.136665);
	var mapOptions = {
		zoom: 13,
		center: sohoOffice,
		mapTypeControl: false,
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		},
		draggable:false,
		scrollwheel: false,
		panControl: false,
		streetViewControl: false
	};
	map = new google.maps.Map(
		document.getElementById('map-canvas'),
		mapOptions
	);

	var marker = new google.maps.Marker({
		position: sohoOffice,
		map: map,
		title: 'Our offices'
	});
}

google.maps.event.addDomListener(window, 'load', initialize);