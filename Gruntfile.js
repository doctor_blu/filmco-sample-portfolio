module.exports = function(grunt) {
  
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    globalConfig: {
      src: "./app/",
      dist: "./dist/"
    },

    connect: {
      all: {
        options:{
          port: 9000,
          hostname: '',
          base:'<%= globalConfig.dist %>',
          livereload: true,
          open:true
        }
      }
    },

    imagemin: {
      baseFolder: {
        files: [{
          expand: true,
          cwd: '<%= globalConfig.src %>img/',
          src: ['*.{png,jpg,gif}'],
          dest: '<%= globalConfig.dist %>img/'
        }]
      },
      gallery: {
        files: [{
          expand: true,
          cwd: '<%= globalConfig.src %>img/gallery/',
          src: ['*.{png,jpg,gif}'],
          dest: '<%= globalConfig.dist %>img/gallery/'
        }]
      },
      carousel: {
        files: [{
          expand: true,
          cwd: '<%= globalConfig.src %>img/carousel/',
          src: ['*.{png,jpg,gif}'],
          dest: '<%= globalConfig.dist %>img/carousel/'
        }]
      }
    },

    sprite:{
      all: {
        src: '<%= globalConfig.src %>img/sprite/*.png',
        destImg: '<%= globalConfig.dist %>img/spritesheet.png',
        destCSS: '<%= globalConfig.src %>scss/spritevars.scss',
        imgPath: '../img/spritesheet.png'
      }
    },

    cssmin: {
      minify: {
        src: [
          './bower_components/normalize-css/normalize.css',
          './bower_components/unsemantic/assets/stylesheets/unsemantic-grid-responsive.css',
          '<%= globalConfig.src %>css/app.css'
        ],
        dest: '<%= globalConfig.dist %>css/app.css'
      }
    },

    sass: {
      dist: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          '<%= globalConfig.src %>css/app.css' : '<%= globalConfig.src %>scss/app.scss'
        }
      }
    },

    uglify: {
      options: {
        // the banner is inserted at the top of the output
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        sourceMap: true
      },
      dist: {
        files: {
          '<%= globalConfig.dist %>js/app.min.js': ['<%= globalConfig.src %>js/*.js']
        }
      }
    },

    jshint: {

      // configure JSHint (documented at http://www.jshint.com/docs/)
      options: {
        curly: true, // put always curly braces around block
        eqeqeq: true, // == and != are not valid, use === and !== instead
        bitwise: true, // prohibits the use of bitwise operators
        //undef: true, // error when a variable is undefined
        trailing: true, // warning when there are trailing whitespaces
        node:true, // defines globals available in the Node runtime environment
        devel:true, // defines globals used for debugging (console, alert..)
        browser:true, // defines globals exposed by modern browsers (document, navigator, FileReader...)
        loopfunc: true
      },

      scripts: '<%= globalConfig.src %>js/*.js',

      gruntfile: 'Gruntfile.js'
    },

    watch: {

      options: {
        livereload: true,
      },

      gruntfile: {
        files: 'Gruntfile.js',
        tasks: ['jshint:gruntfile'],
      },

      sass: {
        files: '<%= globalConfig.src %>scss/**/*.scss',
        tasks: ['stylesheets']
      },

      scripts: {
        files: '<%= globalConfig.src %>js/*.js',
        tasks: ['jshint:scripts', 'uglify']
      },

      html: {
        files: '<%= globalConfig.dist %>index.html',
        options: {
          livereload:true
        }
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-spritesmith');

  grunt.registerTask('images', [
    //'imagemin',
    'sprite'
  ]);

  grunt.registerTask('stylesheets', [
    'sass',
    'cssmin'
  ]);

  grunt.registerTask('build', [
    'images',
    'stylesheets',
    'jshint:gruntfile',
    'jshint:scripts',
    'uglify'
  ]);

  grunt.registerTask('default', [
    'build',
    'connect',
    'watch'
  ]);
};